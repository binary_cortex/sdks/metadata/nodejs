import { Buffer } from 'buffer';
import https from 'https';
import { Response } from './types/common.types';
import fetch, { Response as FetchResponse } from 'node-fetch';

const LMD_URL: string = process.env.LMD_URL ?? 'http://localhost:3000';
const AUTH_URL: string = process.env.AUTH_URL ?? 'https://localhost:3001';

const FORCE_CERTIFICATE: boolean = true;
const authOptions: any = { method: 'POST' };

if (FORCE_CERTIFICATE) {
	authOptions.agent = new https.Agent({
		rejectUnauthorized: false,
	});
}

// export section
// ...

export type MetadataResponse = Response

export interface AccessKey {
    accessId: string;
    accessSecret: string;
}

export interface KeyResponse {
    username?: string;
    accessKey?: AccessKey;
    error?: string;
}

export interface KeysResponse {
    username?: string;
    accessKeys?: AccessKey[];
    error?: string;
}

export async function login(username: string, password: string): Promise<KeyResponse> {
	const response: FetchResponse = await fetch(`${AUTH_URL}/login`, {
		body: JSON.stringify({ username, password }),
		headers: { 'Content-Type': 'application/json' },
		...authOptions,
	});

	return response.ok
		? response.json()
		: { error: await response.text() };
}

export async function createUser(
	firstName: string,
	lastName: string,
	email: string,
	username: string,
	password: string,
): Promise<string> {
	const response: FetchResponse = await fetch(`${AUTH_URL}/createUser`, {
		body: JSON.stringify({
			firstName, lastName, email, username, password,
		}),
		headers: { 'Content-Type': 'application/json' },
		...authOptions,
	});

	return response.text();
}

export async function createKey(key: AccessKey): Promise<KeyResponse> {
	const response: FetchResponse = await fetch(`${AUTH_URL}/createKey`, { headers: { Authorization: `Basic ${keyBase64(key)}` }, ...authOptions });

	return response.ok
		? response.json()
		: { error: await response.text() };
}

export async function getKeys(key: AccessKey): Promise<KeysResponse> {
	const response: FetchResponse = await fetch(`${AUTH_URL}/getKeys`, { headers: { Authorization: `Basic ${keyBase64(key)}` }, ...authOptions });

	return response.ok
		? response.json()
		: { error: await response.text() };
}

export async function revokeKey(key: AccessKey): Promise<string> {
	const response: FetchResponse = await fetch(`${AUTH_URL}/revokeKey`, { headers: { Authorization: `Basic ${keyBase64(key)}` }, ...authOptions });

	return response.text();
}

export async function getMetadata(url: string, key: AccessKey): Promise<MetadataResponse> {
	const objURL: URL = new URL(url);

	const updatedUrl: string = objURL.origin + objURL.pathname + encodeURIComponent(objURL.search);

	const response: FetchResponse = await fetch(`${LMD_URL}/SpinItOn?&url=${updatedUrl}`, {
		headers: { Authorization: `Basic ${keyBase64(key)}` },
	});

	return response.json();
}

export async function reportBadData(url: string, reason: string): Promise<string> {
	const response: FetchResponse = await fetch(`${LMD_URL}/TheNoteYouNeverWrote`, {
		body: JSON.stringify({ url, reason }),
		headers: { 'Content-Type': 'application/json' },
		...authOptions,
	});

	return response.text();
}

// auxiliary functions
// ...

function keyBase64(key: AccessKey): string {
	const buff: Buffer = Buffer.from(`${key.accessId}:${key.accessSecret}`, 'utf-8');
	return buff.toString('base64');
}

export default {
	login, createUser, createKey, getKeys, revokeKey, getMetadata, reportBadData,
};
